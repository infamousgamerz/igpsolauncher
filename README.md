# IGPSOLauncher

IGPSOLauncher is a custom launcher written in C# for the game Phantasy Star Online Blue Burst.

  - Patch server
  - Update notifications
  - Edit all registry values for PSOBB
  - PSOBB Skin Chooser

*Special thanks to the IG Community for their help and motivation on this project.*