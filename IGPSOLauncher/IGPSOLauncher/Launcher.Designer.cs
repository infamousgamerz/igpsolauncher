﻿namespace IGPSOLauncher
{
    partial class Launcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Launcher));
            this.StartGame = new System.Windows.Forms.Button();
            this.Options = new System.Windows.Forms.Button();
            this.SkinChooser = new System.Windows.Forms.ComboBox();
            this.Website = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // StartGame
            // 
            this.StartGame.BackColor = System.Drawing.Color.Black;
            this.StartGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StartGame.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartGame.ForeColor = System.Drawing.Color.DarkRed;
            this.StartGame.Location = new System.Drawing.Point(12, 12);
            this.StartGame.Name = "StartGame";
            this.StartGame.Size = new System.Drawing.Size(659, 51);
            this.StartGame.TabIndex = 0;
            this.StartGame.Text = "Start Game";
            this.StartGame.UseVisualStyleBackColor = false;
            this.StartGame.Click += new System.EventHandler(this.StartGame_Click);
            // 
            // Options
            // 
            this.Options.BackColor = System.Drawing.Color.Black;
            this.Options.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Options.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Options.ForeColor = System.Drawing.Color.DarkRed;
            this.Options.Location = new System.Drawing.Point(12, 84);
            this.Options.Name = "Options";
            this.Options.Size = new System.Drawing.Size(341, 51);
            this.Options.TabIndex = 1;
            this.Options.Text = "Options";
            this.Options.UseVisualStyleBackColor = false;
            this.Options.Click += new System.EventHandler(this.Options_Click);
            // 
            // SkinChooser
            // 
            this.SkinChooser.BackColor = System.Drawing.Color.Black;
            this.SkinChooser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SkinChooser.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SkinChooser.ForeColor = System.Drawing.Color.DarkRed;
            this.SkinChooser.FormattingEnabled = true;
            this.SkinChooser.Items.AddRange(new object[] {
            "None"});
            this.SkinChooser.Location = new System.Drawing.Point(376, 84);
            this.SkinChooser.Name = "SkinChooser";
            this.SkinChooser.Size = new System.Drawing.Size(295, 24);
            this.SkinChooser.TabIndex = 2;
            this.SkinChooser.SelectedIndexChanged += new System.EventHandler(this.SkinChooser_SelectedIndexChanged);
            // 
            // Website
            // 
            this.Website.AutoSize = true;
            this.Website.BackColor = System.Drawing.Color.Transparent;
            this.Website.LinkColor = System.Drawing.Color.DarkRed;
            this.Website.Location = new System.Drawing.Point(625, 126);
            this.Website.Name = "Website";
            this.Website.Size = new System.Drawing.Size(46, 13);
            this.Website.TabIndex = 3;
            this.Website.TabStop = true;
            this.Website.Text = "Website";
            this.Website.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Website_LinkClicked);
            // 
            // Launcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(683, 148);
            this.Controls.Add(this.Website);
            this.Controls.Add(this.SkinChooser);
            this.Controls.Add(this.Options);
            this.Controls.Add(this.StartGame);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Launcher";
            this.Text = "IGPSO Launcher";
            this.Load += new System.EventHandler(this.Launcher_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button StartGame;
        private System.Windows.Forms.Button Options;
        private System.Windows.Forms.ComboBox SkinChooser;
        private System.Windows.Forms.LinkLabel Website;
    }
}

