﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace IGPSOLauncher
{
    public partial class Options : Form
    {

        public static readonly String REGDIR = "Software\\SonicTeam\\PSOBB";

        public Options()
        {
            InitializeComponent();
        }

        private void Options_Load(object sender, EventArgs e)
        {
            //Set language to english
            Language.SelectedIndex = 0;

            //Get fonts and set chosen font
            RegistryKey FontKey = Registry.CurrentUser.OpenSubKey(REGDIR);
            Object FONTJPN = FontKey.GetValue("FONT_JPN");
            int count = 0;
            int SelectedFont = 0;
            foreach (FontFamily font in FontFamily.Families)
            {
                FontOption.Items.Add(font.Name.ToString());

                //Not selecting chosen font!!!  TO-DO!!!
                if (FONTJPN.ToString() == font.Name.ToString())
                {
                    SelectedFont = count;
                }
                count++;

            }
            FontOption.SelectedIndex = SelectedFont;

            //Get credentials
            RegistryKey Credentialskey = Registry.CurrentUser.OpenSubKey(REGDIR);
            int CREDENTIALS = Convert.ToInt32(Credentialskey.GetValue("ACCOUNT_CHECK"));
            if (CREDENTIALS == 1)
            {
                CredentialsSave.Checked = true;
            }
            else
            {
                CredentialsSave.Checked = false;
            }

            //Set resolution values
            Dictionary<int, int> ResolutionMapping = new Dictionary<int, int>
            {
              {0, 0},
              {1, 1},
              {2, 2},
              {3, 4}
            };

            //Get resolution from registry
            RegistryKey CTRLBUFkey = Registry.CurrentUser.OpenSubKey(REGDIR);
            byte[] CTRLBUF = (byte[])CTRLBUFkey.GetValue("CTRLBUF");
            byte[] ChosenResolution = CTRLBUF.Take(1).ToArray();
            string ResolutionByteToString = BitConverter.ToString(ChosenResolution);
            int resolutionInt = int.Parse(ResolutionByteToString);
            if (resolutionInt == 4)
            {
                resolutionInt = 3;
            }
            Resolution.SelectedIndex = resolutionInt;

            //Get color depth from registry
            byte[] ChosenColorDepth = CTRLBUF.Skip(4).Take(1).ToArray();
            string ColorDepthByteToString = BitConverter.ToString(ChosenColorDepth);
            int ColorDepthInt = int.Parse(ColorDepthByteToString);
            ColorDepth.SelectedIndex = ColorDepthInt;

            //Get vsync from registry
            byte[] ChosenVSync = CTRLBUF.Skip(8).Take(1).ToArray();
            string VSyncByteToString = BitConverter.ToString(ChosenVSync);
            int VSyncInt = int.Parse(VSyncByteToString);
            if (VSyncInt == 1)
            {
                VSync.Checked = true;
            }
            else
            {
                VSync.Checked = false;
            }

            //Get window mode from registry
            RegistryKey WindowModekey = Registry.CurrentUser.OpenSubKey(REGDIR);
            int WINDOWMODE = Convert.ToInt32(WindowModekey.GetValue("WINDOW_MODE"));
            if (WINDOWMODE == 1)
            {
                WindowMode.Checked = true;
            }
            else
            {
                WindowMode.Checked = false;
            }

            //Get word wrap
            RegistryKey WordWrapkey = Registry.CurrentUser.OpenSubKey(REGDIR);
            int WORDWRAP = Convert.ToInt32(WindowModekey.GetValue("WORD_WRAP"));
            if (WORDWRAP == 1)
            {
                WordWrap.Checked = true;
            }
            else
            {
                WordWrap.Checked = false;
            }

            //Get graphics
            RegistryKey GRAPHICCTRLkey = Registry.CurrentUser.OpenSubKey(REGDIR);
            byte[] GRAPHICCTRL = (byte[])GRAPHICCTRLkey.GetValue("GRAPHICCTRL");
            byte[] ChosenGRAPHICCTRLLvl = GRAPHICCTRL.Take(1).ToArray();

            //Advanced effect
            byte[] ChosenGRAPHICCTRLAdvancedEffect = GRAPHICCTRL.Skip(4).Take(1).ToArray();
            string AdvancedEffectByteToString = BitConverter.ToString(ChosenGRAPHICCTRLAdvancedEffect);
            int AdvancedEffectInt = int.Parse(AdvancedEffectByteToString);
            if (AdvancedEffectInt == 1)
            {
                AdvancedEffect.Checked = true;
            }
            else
            {
                AdvancedEffect.Checked = false;
            }

            //Shadow
            byte[] ChosenGRAPHICCTRLShadow = GRAPHICCTRL.Skip(8).Take(1).ToArray();
            string ShadowByteToString = BitConverter.ToString(ChosenGRAPHICCTRLShadow);
            int ShadowInt = int.Parse(ShadowByteToString);
            this.Shadow.Value = ShadowInt;

            //Enemy
            byte[] ChosenGRAPHICCTRLEnemy = GRAPHICCTRL.Skip(12).Take(1).ToArray();
            string EnemyByteToString = BitConverter.ToString(ChosenGRAPHICCTRLEnemy);
            int EnemyInt = int.Parse(EnemyByteToString);
            this.Enemy.Value = EnemyInt;

            //Map
            byte[] ChosenGRAPHICCTRLMap = GRAPHICCTRL.Skip(16).Take(1).ToArray();
            string MapByteToString = BitConverter.ToString(ChosenGRAPHICCTRLMap);
            int MapInt = int.Parse(MapByteToString);
            this.Map.Value = MapInt;

            //Clip distance
            byte[] ChosenGRAPHICCTRLClipDistance = GRAPHICCTRL.Skip(20).Take(1).ToArray();
            string ClipDistanceByteToString = BitConverter.ToString(ChosenGRAPHICCTRLClipDistance);
            int ClipDistanceInt = int.Parse(ClipDistanceByteToString);
            this.ClipDistance.Value = ClipDistanceInt;

            //Fog
            byte[] ChosenGRAPHICCTRLFog = GRAPHICCTRL.Skip(24).Take(1).ToArray();
            string FogByteToString = BitConverter.ToString(ChosenGRAPHICCTRLFog);
            int FogInt = int.Parse(FogByteToString);
            Fog.SelectedIndex = FogInt;

            //Low textures
            byte[] ChosenGRAPHICCTRLLowResTextures = GRAPHICCTRL.Skip(28).Take(1).ToArray();
            string LowTexturesByteToString = BitConverter.ToString(ChosenGRAPHICCTRLLowResTextures);
            int LowResTexInt = int.Parse(LowTexturesByteToString);
            if (LowResTexInt == 1)
            {
                LowResolutionTextures.Checked = true;
            }
            else
            {
                LowResolutionTextures.Checked = false;
            }

            //Frame skip
            byte[] ChosenGRAPHICCTRLFrameSkip = GRAPHICCTRL.Skip(32).Take(1).ToArray();
            string FrameSkipByteToString = BitConverter.ToString(ChosenGRAPHICCTRLFrameSkip);
            int FrameSkipInt = int.Parse(FrameSkipByteToString);
            FrameSkip.SelectedIndex = FrameSkipInt;

            //Auto Frame skip
            byte[] ChosenGRAPHICCTRLAutoFrameSkip = GRAPHICCTRL.Skip(34).Take(4).ToArray();
            string AutoFrameSkipByteToString = BitConverter.ToString(ChosenGRAPHICCTRLAutoFrameSkip);
            Byte[] AutoFrameSkipHex = new byte[]{
                        0xFF, 0xFF
                    };
            if (AutoFrameSkipByteToString == "FF-FF")
            {
                AutoFrameSkip.Checked = true;
            }
            else
            {
                AutoFrameSkip.Checked = false;
            }

            //Get sounds
            RegistryKey SOUNDCTRLkey = Registry.CurrentUser.OpenSubKey(REGDIR);
            byte[] SOUNDCTRL = (byte[])GRAPHICCTRLkey.GetValue("SOUNDCTRL");

            //All sounds
            byte[] ChosenSOUNDCTRLAll = SOUNDCTRL.Take(1).ToArray();
            string SoundAllByteToString = BitConverter.ToString(ChosenSOUNDCTRLAll);
            int SoundAllInt = int.Parse(SoundAllByteToString);
            if (SoundAllInt == 1)
            {
                SoundCheck.Checked = true;
            }
            else
            {
                SoundCheck.Checked = false;
            }

            //BGM
            byte[] ChosenSOUNDCTRLBGM = SOUNDCTRL.Skip(4).Take(1).ToArray();
            string BGMByteToString = BitConverter.ToString(ChosenSOUNDCTRLBGM);
            int BGMInt = int.Parse(BGMByteToString);
            if (BGMInt == 1)
            {
                BGMSound.Checked = true;
            }
            else
            {
                BGMSound.Checked = false;
            }

            //Sound Effects
            byte[] ChosenSOUNDCTRLSE = SOUNDCTRL.Skip(8).Take(1).ToArray();
            string SEByteToString = BitConverter.ToString(ChosenSOUNDCTRLSE);
            int SEInt = int.Parse(SEByteToString);
            if (SEInt == 1)
            {
                SESound.Checked = true;
            }
            else
            {
                SESound.Checked = false;
            }
        }

        private void OptionsSave_Click(object sender, EventArgs e)
        {
            //Language - Coming soon

            //Font
            using (var myKey = Registry.CurrentUser.OpenSubKey(REGDIR))
            {
                if (myKey != null)
                {
                    String value = FontOption.Text;
                    RegistryKey key = Registry.CurrentUser.CreateSubKey(REGDIR);
                    key.SetValue("FONT_JPN", value, RegistryValueKind.String);
                }
            }

            //Credentials
            if (CredentialsSave.Checked)
            {
                using (var myKey = Registry.CurrentUser.OpenSubKey(REGDIR))
                {
                    if (myKey != null)
                    {
                        int value = 1;
                        RegistryKey key = Registry.CurrentUser.CreateSubKey(REGDIR);
                        key.SetValue("ACCOUNT_CHECK", value, RegistryValueKind.DWord);
                    }
                }
            }
            else
            {
                using (var myKey = Registry.CurrentUser.OpenSubKey(REGDIR))
                {
                    if (myKey != null)
                    {
                        int value = 0;
                        RegistryKey key = Registry.CurrentUser.CreateSubKey(REGDIR);
                        key.SetValue("ACCOUNT_CHECK", value, RegistryValueKind.DWord);
                    }
                }
            }

            //CTRLBUF
            using (var myKey = Registry.CurrentUser.OpenSubKey(REGDIR))
            {
                //Resolution
                int ChosenResolution = Resolution.SelectedIndex;
                if (ChosenResolution == 3)
                {
                    ChosenResolution = 4;
                }
                byte ChosenResolutionFinal = Convert.ToByte(ChosenResolution);

                //Color depth
                int ChosenColorDepth = ColorDepth.SelectedIndex;
                byte ChosenColorDepthFinal = Convert.ToByte(ChosenColorDepth);

                //Vsync
                byte[] VSyncFinal = new byte[]{};
                if (VSync.Checked == true)
                {
                    VSyncFinal = new byte[]{
                        0x01
                    };
                }
                else
                {
                    VSyncFinal = new byte[]{
                        0x00
                    };
                }
                byte[] ChosenVSyncFinal = VSyncFinal;

                //Save CTRLBUF
                if (myKey != null)
                {
                    Byte[] value = new byte[]{
                        ChosenResolutionFinal,0x00,0x00,0x00,
                        ChosenColorDepthFinal,0x00,0x00,0x00,
                        ChosenVSyncFinal[0],0x00,0x00,0x00
                    };
                    RegistryKey key = Registry.CurrentUser.CreateSubKey(REGDIR);
                    key.SetValue("CTRLBUF", value, RegistryValueKind.Binary);
                }
            }

            //GRAPHICCTRL
            using (var myKey = Registry.CurrentUser.OpenSubKey(REGDIR))
            {
                //Advanced effect
                byte[] AdvancedEffectFinal = new byte[] { };
                if (AdvancedEffect.Checked == true)
                {
                    AdvancedEffectFinal = new byte[]{
                        0x01
                    };
                }
                else
                {
                    AdvancedEffectFinal = new byte[]{
                        0x00
                    };
                }
                byte[] ChosenAdvancedEffectFinal = AdvancedEffectFinal;

                //Shadows
                byte[] ShadowFinal = new byte[]{};
                if (Shadow.Value == 0)
                {
                    ShadowFinal = new byte[]{
                        0x00
                    };
                }
                else if (Shadow.Value == 1)
                {
                    ShadowFinal = new byte[]{
                        0x01
                    };
                }
                else
                {
                    ShadowFinal = new byte[]{
                        0x02
                    };
                }
                byte[] ChosenShadowFinal = ShadowFinal;

                //Enemy
                byte[] EnemyFinal = new byte[]{};
                if (Enemy.Value == 0)
                {
                    EnemyFinal = new byte[]{
                        0x00
                    };
                }
                else
                {
                    EnemyFinal = new byte[]{
                        0x01
                    };
                }
                byte[] ChosenEnemyFinal = EnemyFinal;

                //Map
                byte[] MapFinal = new byte[]{};
                if (Map.Value == 0)
                {
                    MapFinal = new byte[]{
                        0x00
                    };
                }
                else if (Map.Value == 1)
                {
                    MapFinal = new byte[]{
                        0x01
                    };
                }
                else
                {
                    MapFinal = new byte[]{
                        0x02
                    };
                }
                byte[] ChosenMapFinal = MapFinal;

                //Clip distance
                byte[] ClipDistanceFinal = new byte[]{};
                if (ClipDistance.Value == 0)
                {
                    ClipDistanceFinal = new byte[]{
                        0x00
                    };
                }
                else if (ClipDistance.Value == 1)
                {
                    ClipDistanceFinal = new byte[]{
                        0x01
                    };
                }
                else
                {
                    ClipDistanceFinal = new byte[]{
                        0x02
                    };
                }
                byte[] ChosenClipDistanceFinal = ClipDistanceFinal;

                //Fog
                int ChosenFog = Fog.SelectedIndex;
                byte ChosenFogFinal = Convert.ToByte(ChosenFog);

                //Low textures
                byte[] LowTexturesFinal = new byte[]{};
                if (LowResolutionTextures.Checked == true)
                {
                    LowTexturesFinal = new byte[]{
                        0x01
                    };
                }
                else
                {
                    LowTexturesFinal = new byte[]{
                        0x00
                    };
                }
                byte[] ChosenLowTexturesFinal = LowTexturesFinal;

                //Frame skip
                int ChosenFrameSkip = FrameSkip.SelectedIndex;
                byte ChosenFrameSkipFinal = Convert.ToByte(ChosenFrameSkip);

                //Auto frame skip
                byte[] AutoFrameSkipFinal = new byte[]{};
                if (AutoFrameSkip.Checked == true)
                {
                    AutoFrameSkipFinal = new byte[]{
                        0xFF,0xFF
                    };
                }
                else
                {
                    AutoFrameSkipFinal = new byte[]{
                        0x00,0x00
                    };
                }
                byte[] ChosenAutoFrameSkipFinal = AutoFrameSkipFinal;

                //Save GRAPHICCTRL
                if (myKey != null)
                {
                    Byte[] value = new byte[]{
                        0x03,0x00,0x00,0x00, //Custom graphics
                        ChosenAdvancedEffectFinal[0],0x00,0x00,0x00,
                        ChosenShadowFinal[0],0x00,0x00,0x00,
                        ChosenEnemyFinal[0],0x00,0x00,0x00,
                        ChosenMapFinal[0],0x00,0x00,0x00,
                        ChosenClipDistanceFinal[0],0x00,0x00,0x00,
                        ChosenFogFinal,0x00,0x00,0x00,
                        ChosenLowTexturesFinal[0],0x00,0x00,0x00,
                        ChosenFrameSkipFinal,0x00,
                        ChosenAutoFrameSkipFinal[0],ChosenAutoFrameSkipFinal[1]
                    };
                    RegistryKey key = Registry.CurrentUser.CreateSubKey(REGDIR);
                    key.SetValue("GRAPHICCTRL", value, RegistryValueKind.Binary);
                }

                //Sound
                byte[] SoundCheckFinal = new byte[]{};
                if (SoundCheck.Checked == true)
                {
                    SoundCheckFinal = new byte[]{
                        0x01
                    };
                }
                else
                {
                    SoundCheckFinal = new byte[]{
                        0x00
                    };
                }
                byte[] ChosenSoundCheckFinal = SoundCheckFinal;

                //BGM
                byte[] BGMSoundFinal = new byte[]{};
                if (BGMSound.Checked == true)
                {
                    BGMSoundFinal = new byte[]{
                        0x01
                    };
                }
                else
                {
                    BGMSoundFinal = new byte[]{
                        0x00
                    };
                }
                byte[] ChosenBGMSoundFinal = BGMSoundFinal;

                //Sound Effects
                byte[] SESoundFinal = new byte[]{};
                if (SESound.Checked == true)
                {
                    SESoundFinal = new byte[]{
                        0x01
                    };
                }
                else
                {
                    SESoundFinal = new byte[]{
                        0x00
                    };
                }
                byte[] ChosenSESoundFinal = SESoundFinal;

                //Save SOUNDCTRL
                if (myKey != null)
                {
                    Byte[] value = new byte[]{
                        ChosenSoundCheckFinal[0],0x00,0x00,0x00,
                        ChosenBGMSoundFinal[0],0x00,0x00,0x00,
                        ChosenSESoundFinal[0],0x00,0x00,0x00
                    };
                    RegistryKey key = Registry.CurrentUser.CreateSubKey(REGDIR);
                    key.SetValue("SOUNDCTRL", value, RegistryValueKind.Binary);
                }
            }

            //Window mode
            if (WindowMode.Checked == true)
            {
                using (var myKey = Registry.CurrentUser.OpenSubKey(REGDIR))
                {
                    if (myKey != null)
                    {
                        int value = 1;
                        RegistryKey key = Registry.CurrentUser.CreateSubKey(REGDIR);
                        key.SetValue("WINDOW_MODE", value, RegistryValueKind.DWord);
                    }
                }
            }
            else
            {
                using (var myKey = Registry.CurrentUser.OpenSubKey(REGDIR))
                {
                    if (myKey != null)
                    {
                        int value = 0;
                        RegistryKey key = Registry.CurrentUser.CreateSubKey(REGDIR);
                        key.SetValue("WINDOW_MODE", value, RegistryValueKind.DWord);
                    }
                }
            }

            //Word wrap
            if (WordWrap.Checked == true)
            {
                using (var myKey = Registry.CurrentUser.OpenSubKey(REGDIR))
                {
                    if (myKey != null)
                    {
                        int value = 1;
                        RegistryKey key = Registry.CurrentUser.CreateSubKey(REGDIR);
                        key.SetValue("WORD_WRAP", value, RegistryValueKind.DWord);
                    }
                }
            }
            else
            {
                using (var myKey = Registry.CurrentUser.OpenSubKey(REGDIR))
                {
                    if (myKey != null)
                    {
                        int value = 0;
                        RegistryKey key = Registry.CurrentUser.CreateSubKey(REGDIR);
                        key.SetValue("WORD_WRAP", value, RegistryValueKind.DWord);
                    }
                }
            }


            this.Close();
        }

        private void OptionsCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}