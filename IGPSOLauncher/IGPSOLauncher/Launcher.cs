﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IGPSOLauncher
{
    public partial class Launcher : Form
    {
        public Launcher()
        {
            InitializeComponent();
        }

        private void Launcher_Load(object sender, EventArgs e)
        {
            //Launcher version
            string LAUNCHER_VERSION = "0.2";

            //Launcher version check
            using (WebClient client = new WebClient())
            {
                using (Stream stream = client.OpenRead("http://pso.infamousgamerz.net/launcher/launcher.txt"))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if(line != LAUNCHER_VERSION)
                            {
                                MessageBox.Show("There is a new version of the launcher.  Please update.", "Version Update");
                                System.Diagnostics.Process.Start("http://pso.infamousgamerz.net/launcher/"+line+"/IGPSOLauncher.exe");
                                this.Close();
                            }
                        }
                    }
                }
            }

            var PSOdir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (!File.Exists(PSOdir + "\\Psobb.exe"))
            {
                MessageBox.Show("You need to place this executable in the directory of your Psobb.exe!", "Uh-oh!");
                //this.Close();  //Removed for anyone wanting to see the program.
            }

            //Skin chooser
            var folder = "skins";
            var dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (!Directory.Exists(dir + "\\" + folder))
            {
                Directory.CreateDirectory(dir + "\\" + folder);
            }
            var skinDir = dir + "\\" + folder;
            var directories = Directory.GetDirectories(skinDir);
            foreach (string directory in directories)
            {
                SkinChooser.Items.Add(Path.GetFileName(directory));
            }
        }

        private void StartGame_Click(object sender, EventArgs e)
        {
            StartGame.Text = "Patching...";
            using (WebClient client = new WebClient())
            {
                using (Stream stream = client.OpenRead("http://pso.infamousgamerz.net/patch/patch.txt"))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            WebClient Client = new WebClient();
                            var folder = Path.GetDirectoryName(line);
                            var dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                            if (!Directory.Exists(dir + "\\" + folder))
                            {
                                Directory.CreateDirectory(dir + "\\" + folder);
                            }
                            if(!File.Exists(dir + line))
                            {
                                Client.DownloadFile("http://pso.infamousgamerz.net/patch/" + line, dir + "\\" + line);
                            }
                        }
                    }
                }
            }
            System.Diagnostics.Process.Start("Psobb.exe");
            this.Close();
        }

        private void Options_Click(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.Start("option.exe");
            Options options = new Options();
            options.ShowDialog();
        }

        private void Website_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://infamousgamerz.net");
        }

        private void SkinChooser_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Backup
            var dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (!Directory.Exists(dir + "\\skins\\default"))
            {

                DialogResult BackupDialogResult = MessageBox.Show("In order to continue you must backup your data folder.  Do that now?  It may take a moment.", "Backup", MessageBoxButtons.YesNo);
                if (BackupDialogResult == DialogResult.Yes)
                {
                    Directory.CreateDirectory(dir + "\\skins\\default");

                    string[] filePaths = Directory.GetFiles(dir + "\\data");
                    foreach (var filename in filePaths)
                    {
                        string fileName = Path.GetFileName(filename).ToString();
                        string filePath = filename.ToString();

                        string str = dir + "\\skins\\default\\" + fileName;
                        if (!File.Exists(str))
                        {
                            File.Copy(filePath, str);
                        }
                    }
                }
                else if (BackupDialogResult == DialogResult.No)
                {
                    MessageBox.Show("Sorry, you can't use skins without making a backup.", "Uh-oh!");
                    this.Close();
                }
            }

            //Switch skins
            if(SkinChooser.SelectedIndex != 0)
            {
                string skin = SkinChooser.Text;
                DialogResult SkinChooserDialogResult = MessageBox.Show("Are you sure you want to enable the " + skin + " skin?  This will overwrite files in your data folder.", "Confirmation", MessageBoxButtons.YesNo);
                if (SkinChooserDialogResult == DialogResult.Yes)
                {
                    DirectoryCopy(dir + "\\skins\\" + skin, dir + "\\data", true);
                    MessageBox.Show("PSOBB skin updated!", "Skin Updated!");
                }
                else if (SkinChooserDialogResult == DialogResult.No)
                {
                    MessageBox.Show("OK.  Skin was not updated.", "OK");
                }
            } 

        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }
}
