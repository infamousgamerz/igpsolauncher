﻿namespace IGPSOLauncher
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Options));
            this.OptionsSave = new System.Windows.Forms.Button();
            this.OptionsCancel = new System.Windows.Forms.Button();
            this.TabDisplay = new System.Windows.Forms.TabControl();
            this.OptionsGeneral = new System.Windows.Forms.TabPage();
            this.CredentialsSave = new System.Windows.Forms.CheckBox();
            this.FontOption = new System.Windows.Forms.ComboBox();
            this.FontLabel = new System.Windows.Forms.Label();
            this.Language = new System.Windows.Forms.ComboBox();
            this.LanguageLabel = new System.Windows.Forms.Label();
            this.OptionsDisplay = new System.Windows.Forms.TabPage();
            this.ColorDepth = new System.Windows.Forms.ComboBox();
            this.ColorDepthLabel = new System.Windows.Forms.Label();
            this.VSync = new System.Windows.Forms.CheckBox();
            this.WordWrap = new System.Windows.Forms.CheckBox();
            this.WindowMode = new System.Windows.Forms.CheckBox();
            this.ResolutionLabel = new System.Windows.Forms.Label();
            this.Resolution = new System.Windows.Forms.ComboBox();
            this.Graphics = new System.Windows.Forms.TabPage();
            this.AutoFrameSkip = new System.Windows.Forms.CheckBox();
            this.LowResolutionTextures = new System.Windows.Forms.CheckBox();
            this.ClipDistance = new System.Windows.Forms.TrackBar();
            this.ClipDistanceLabel = new System.Windows.Forms.Label();
            this.Fog = new System.Windows.Forms.ComboBox();
            this.FogLabel = new System.Windows.Forms.Label();
            this.AdvancedEffect = new System.Windows.Forms.CheckBox();
            this.Map = new System.Windows.Forms.TrackBar();
            this.MapLabel = new System.Windows.Forms.Label();
            this.Enemy = new System.Windows.Forms.TrackBar();
            this.EnemyLabel = new System.Windows.Forms.Label();
            this.ShadowLabel = new System.Windows.Forms.Label();
            this.Shadow = new System.Windows.Forms.TrackBar();
            this.FrameSkip = new System.Windows.Forms.ComboBox();
            this.FrameSkipLabel = new System.Windows.Forms.Label();
            this.Sound = new System.Windows.Forms.TabPage();
            this.SESound = new System.Windows.Forms.CheckBox();
            this.BGMSound = new System.Windows.Forms.CheckBox();
            this.SoundCheck = new System.Windows.Forms.CheckBox();
            this.TabDisplay.SuspendLayout();
            this.OptionsGeneral.SuspendLayout();
            this.OptionsDisplay.SuspendLayout();
            this.Graphics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClipDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Map)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Enemy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Shadow)).BeginInit();
            this.Sound.SuspendLayout();
            this.SuspendLayout();
            // 
            // OptionsSave
            // 
            this.OptionsSave.Location = new System.Drawing.Point(385, 181);
            this.OptionsSave.Name = "OptionsSave";
            this.OptionsSave.Size = new System.Drawing.Size(75, 23);
            this.OptionsSave.TabIndex = 0;
            this.OptionsSave.Text = "Save";
            this.OptionsSave.UseVisualStyleBackColor = true;
            this.OptionsSave.Click += new System.EventHandler(this.OptionsSave_Click);
            // 
            // OptionsCancel
            // 
            this.OptionsCancel.Location = new System.Drawing.Point(475, 181);
            this.OptionsCancel.Name = "OptionsCancel";
            this.OptionsCancel.Size = new System.Drawing.Size(75, 23);
            this.OptionsCancel.TabIndex = 1;
            this.OptionsCancel.Text = "Cancel";
            this.OptionsCancel.UseVisualStyleBackColor = true;
            this.OptionsCancel.Click += new System.EventHandler(this.OptionsCancel_Click);
            // 
            // TabDisplay
            // 
            this.TabDisplay.Controls.Add(this.OptionsGeneral);
            this.TabDisplay.Controls.Add(this.OptionsDisplay);
            this.TabDisplay.Controls.Add(this.Graphics);
            this.TabDisplay.Controls.Add(this.Sound);
            this.TabDisplay.Location = new System.Drawing.Point(13, 13);
            this.TabDisplay.Name = "TabDisplay";
            this.TabDisplay.SelectedIndex = 0;
            this.TabDisplay.Size = new System.Drawing.Size(537, 162);
            this.TabDisplay.TabIndex = 2;
            // 
            // OptionsGeneral
            // 
            this.OptionsGeneral.Controls.Add(this.CredentialsSave);
            this.OptionsGeneral.Controls.Add(this.FontOption);
            this.OptionsGeneral.Controls.Add(this.FontLabel);
            this.OptionsGeneral.Controls.Add(this.Language);
            this.OptionsGeneral.Controls.Add(this.LanguageLabel);
            this.OptionsGeneral.Location = new System.Drawing.Point(4, 22);
            this.OptionsGeneral.Name = "OptionsGeneral";
            this.OptionsGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.OptionsGeneral.Size = new System.Drawing.Size(529, 136);
            this.OptionsGeneral.TabIndex = 0;
            this.OptionsGeneral.Text = "General";
            this.OptionsGeneral.UseVisualStyleBackColor = true;
            // 
            // CredentialsSave
            // 
            this.CredentialsSave.AutoSize = true;
            this.CredentialsSave.Location = new System.Drawing.Point(13, 68);
            this.CredentialsSave.Name = "CredentialsSave";
            this.CredentialsSave.Size = new System.Drawing.Size(172, 17);
            this.CredentialsSave.TabIndex = 4;
            this.CredentialsSave.Text = "Save Username and Password";
            this.CredentialsSave.UseVisualStyleBackColor = true;
            // 
            // FontOption
            // 
            this.FontOption.FormattingEnabled = true;
            this.FontOption.Location = new System.Drawing.Point(68, 39);
            this.FontOption.Name = "FontOption";
            this.FontOption.Size = new System.Drawing.Size(121, 21);
            this.FontOption.TabIndex = 3;
            // 
            // FontLabel
            // 
            this.FontLabel.AutoSize = true;
            this.FontLabel.Location = new System.Drawing.Point(10, 39);
            this.FontLabel.Name = "FontLabel";
            this.FontLabel.Size = new System.Drawing.Size(28, 13);
            this.FontLabel.TabIndex = 2;
            this.FontLabel.Text = "Font";
            // 
            // Language
            // 
            this.Language.FormattingEnabled = true;
            this.Language.Items.AddRange(new object[] {
            "English"});
            this.Language.Location = new System.Drawing.Point(68, 8);
            this.Language.Name = "Language";
            this.Language.Size = new System.Drawing.Size(121, 21);
            this.Language.TabIndex = 1;
            // 
            // LanguageLabel
            // 
            this.LanguageLabel.AutoSize = true;
            this.LanguageLabel.Location = new System.Drawing.Point(7, 11);
            this.LanguageLabel.Name = "LanguageLabel";
            this.LanguageLabel.Size = new System.Drawing.Size(55, 13);
            this.LanguageLabel.TabIndex = 0;
            this.LanguageLabel.Text = "Language";
            // 
            // OptionsDisplay
            // 
            this.OptionsDisplay.Controls.Add(this.ColorDepth);
            this.OptionsDisplay.Controls.Add(this.ColorDepthLabel);
            this.OptionsDisplay.Controls.Add(this.VSync);
            this.OptionsDisplay.Controls.Add(this.WordWrap);
            this.OptionsDisplay.Controls.Add(this.WindowMode);
            this.OptionsDisplay.Controls.Add(this.ResolutionLabel);
            this.OptionsDisplay.Controls.Add(this.Resolution);
            this.OptionsDisplay.Location = new System.Drawing.Point(4, 22);
            this.OptionsDisplay.Name = "OptionsDisplay";
            this.OptionsDisplay.Padding = new System.Windows.Forms.Padding(3);
            this.OptionsDisplay.Size = new System.Drawing.Size(529, 136);
            this.OptionsDisplay.TabIndex = 1;
            this.OptionsDisplay.Text = "Display";
            this.OptionsDisplay.UseVisualStyleBackColor = true;
            // 
            // ColorDepth
            // 
            this.ColorDepth.FormattingEnabled = true;
            this.ColorDepth.Items.AddRange(new object[] {
            "16Bit",
            "32Bit"});
            this.ColorDepth.Location = new System.Drawing.Point(77, 37);
            this.ColorDepth.Name = "ColorDepth";
            this.ColorDepth.Size = new System.Drawing.Size(121, 21);
            this.ColorDepth.TabIndex = 9;
            // 
            // ColorDepthLabel
            // 
            this.ColorDepthLabel.AutoSize = true;
            this.ColorDepthLabel.Location = new System.Drawing.Point(7, 42);
            this.ColorDepthLabel.Name = "ColorDepthLabel";
            this.ColorDepthLabel.Size = new System.Drawing.Size(63, 13);
            this.ColorDepthLabel.TabIndex = 8;
            this.ColorDepthLabel.Text = "Color Depth";
            // 
            // VSync
            // 
            this.VSync.AutoSize = true;
            this.VSync.Location = new System.Drawing.Point(10, 111);
            this.VSync.Name = "VSync";
            this.VSync.Size = new System.Drawing.Size(60, 17);
            this.VSync.TabIndex = 7;
            this.VSync.Text = "V-Sync";
            this.VSync.UseVisualStyleBackColor = true;
            // 
            // WordWrap
            // 
            this.WordWrap.AutoSize = true;
            this.WordWrap.Location = new System.Drawing.Point(10, 87);
            this.WordWrap.Name = "WordWrap";
            this.WordWrap.Size = new System.Drawing.Size(81, 17);
            this.WordWrap.TabIndex = 6;
            this.WordWrap.Text = "Word Wrap";
            this.WordWrap.UseVisualStyleBackColor = true;
            // 
            // WindowMode
            // 
            this.WindowMode.AutoSize = true;
            this.WindowMode.Location = new System.Drawing.Point(10, 64);
            this.WindowMode.Name = "WindowMode";
            this.WindowMode.Size = new System.Drawing.Size(95, 17);
            this.WindowMode.TabIndex = 5;
            this.WindowMode.Text = "Window Mode";
            this.WindowMode.UseVisualStyleBackColor = true;
            // 
            // ResolutionLabel
            // 
            this.ResolutionLabel.AutoSize = true;
            this.ResolutionLabel.Location = new System.Drawing.Point(7, 10);
            this.ResolutionLabel.Name = "ResolutionLabel";
            this.ResolutionLabel.Size = new System.Drawing.Size(57, 13);
            this.ResolutionLabel.TabIndex = 3;
            this.ResolutionLabel.Text = "Resolution";
            // 
            // Resolution
            // 
            this.Resolution.FormattingEnabled = true;
            this.Resolution.Items.AddRange(new object[] {
            "640x480",
            "800x600",
            "1024x768",
            "1280x1024"});
            this.Resolution.Location = new System.Drawing.Point(77, 6);
            this.Resolution.Name = "Resolution";
            this.Resolution.Size = new System.Drawing.Size(121, 21);
            this.Resolution.TabIndex = 2;
            // 
            // Graphics
            // 
            this.Graphics.Controls.Add(this.AutoFrameSkip);
            this.Graphics.Controls.Add(this.LowResolutionTextures);
            this.Graphics.Controls.Add(this.ClipDistance);
            this.Graphics.Controls.Add(this.ClipDistanceLabel);
            this.Graphics.Controls.Add(this.Fog);
            this.Graphics.Controls.Add(this.FogLabel);
            this.Graphics.Controls.Add(this.AdvancedEffect);
            this.Graphics.Controls.Add(this.Map);
            this.Graphics.Controls.Add(this.MapLabel);
            this.Graphics.Controls.Add(this.Enemy);
            this.Graphics.Controls.Add(this.EnemyLabel);
            this.Graphics.Controls.Add(this.ShadowLabel);
            this.Graphics.Controls.Add(this.Shadow);
            this.Graphics.Controls.Add(this.FrameSkip);
            this.Graphics.Controls.Add(this.FrameSkipLabel);
            this.Graphics.Location = new System.Drawing.Point(4, 22);
            this.Graphics.Name = "Graphics";
            this.Graphics.Padding = new System.Windows.Forms.Padding(3);
            this.Graphics.Size = new System.Drawing.Size(529, 136);
            this.Graphics.TabIndex = 2;
            this.Graphics.Text = "Graphics";
            this.Graphics.UseVisualStyleBackColor = true;
            // 
            // AutoFrameSkip
            // 
            this.AutoFrameSkip.AutoSize = true;
            this.AutoFrameSkip.Location = new System.Drawing.Point(201, 9);
            this.AutoFrameSkip.Name = "AutoFrameSkip";
            this.AutoFrameSkip.Size = new System.Drawing.Size(104, 17);
            this.AutoFrameSkip.TabIndex = 14;
            this.AutoFrameSkip.Text = "Auto Frame Skip";
            this.AutoFrameSkip.UseVisualStyleBackColor = true;
            // 
            // LowResolutionTextures
            // 
            this.LowResolutionTextures.AutoSize = true;
            this.LowResolutionTextures.Location = new System.Drawing.Point(125, 111);
            this.LowResolutionTextures.Name = "LowResolutionTextures";
            this.LowResolutionTextures.Size = new System.Drawing.Size(143, 17);
            this.LowResolutionTextures.TabIndex = 13;
            this.LowResolutionTextures.Text = "Low Resolution Textures";
            this.LowResolutionTextures.UseVisualStyleBackColor = true;
            // 
            // ClipDistance
            // 
            this.ClipDistance.Location = new System.Drawing.Point(73, 63);
            this.ClipDistance.Maximum = 2;
            this.ClipDistance.Name = "ClipDistance";
            this.ClipDistance.Size = new System.Drawing.Size(104, 45);
            this.ClipDistance.TabIndex = 12;
            // 
            // ClipDistanceLabel
            // 
            this.ClipDistanceLabel.AutoSize = true;
            this.ClipDistanceLabel.Location = new System.Drawing.Point(7, 69);
            this.ClipDistanceLabel.Name = "ClipDistanceLabel";
            this.ClipDistanceLabel.Size = new System.Drawing.Size(69, 13);
            this.ClipDistanceLabel.TabIndex = 11;
            this.ClipDistanceLabel.Text = "Clip Distance";
            // 
            // Fog
            // 
            this.Fog.FormattingEnabled = true;
            this.Fog.Items.AddRange(new object[] {
            "Vertex Fog",
            "Pixel Fog",
            "Emulation Fog"});
            this.Fog.Location = new System.Drawing.Point(73, 36);
            this.Fog.Name = "Fog";
            this.Fog.Size = new System.Drawing.Size(121, 21);
            this.Fog.TabIndex = 10;
            // 
            // FogLabel
            // 
            this.FogLabel.AutoSize = true;
            this.FogLabel.Location = new System.Drawing.Point(10, 36);
            this.FogLabel.Name = "FogLabel";
            this.FogLabel.Size = new System.Drawing.Size(25, 13);
            this.FogLabel.TabIndex = 9;
            this.FogLabel.Text = "Fog";
            // 
            // AdvancedEffect
            // 
            this.AdvancedEffect.AutoSize = true;
            this.AdvancedEffect.Location = new System.Drawing.Point(13, 111);
            this.AdvancedEffect.Name = "AdvancedEffect";
            this.AdvancedEffect.Size = new System.Drawing.Size(106, 17);
            this.AdvancedEffect.TabIndex = 8;
            this.AdvancedEffect.Text = "Advanced Effect";
            this.AdvancedEffect.UseVisualStyleBackColor = true;
            // 
            // Map
            // 
            this.Map.Location = new System.Drawing.Point(419, 88);
            this.Map.Maximum = 2;
            this.Map.Name = "Map";
            this.Map.Size = new System.Drawing.Size(104, 45);
            this.Map.TabIndex = 7;
            // 
            // MapLabel
            // 
            this.MapLabel.AutoSize = true;
            this.MapLabel.Location = new System.Drawing.Point(381, 88);
            this.MapLabel.Name = "MapLabel";
            this.MapLabel.Size = new System.Drawing.Size(28, 13);
            this.MapLabel.TabIndex = 6;
            this.MapLabel.Text = "Map";
            // 
            // Enemy
            // 
            this.Enemy.Location = new System.Drawing.Point(419, 50);
            this.Enemy.Maximum = 1;
            this.Enemy.Name = "Enemy";
            this.Enemy.Size = new System.Drawing.Size(104, 45);
            this.Enemy.TabIndex = 5;
            // 
            // EnemyLabel
            // 
            this.EnemyLabel.AutoSize = true;
            this.EnemyLabel.Location = new System.Drawing.Point(370, 50);
            this.EnemyLabel.Name = "EnemyLabel";
            this.EnemyLabel.Size = new System.Drawing.Size(39, 13);
            this.EnemyLabel.TabIndex = 4;
            this.EnemyLabel.Text = "Enemy";
            // 
            // ShadowLabel
            // 
            this.ShadowLabel.AutoSize = true;
            this.ShadowLabel.Location = new System.Drawing.Point(370, 9);
            this.ShadowLabel.Name = "ShadowLabel";
            this.ShadowLabel.Size = new System.Drawing.Size(46, 13);
            this.ShadowLabel.TabIndex = 3;
            this.ShadowLabel.Text = "Shadow";
            // 
            // Shadow
            // 
            this.Shadow.Location = new System.Drawing.Point(419, 5);
            this.Shadow.Maximum = 2;
            this.Shadow.Name = "Shadow";
            this.Shadow.Size = new System.Drawing.Size(104, 45);
            this.Shadow.TabIndex = 2;
            // 
            // FrameSkip
            // 
            this.FrameSkip.FormattingEnabled = true;
            this.FrameSkip.Items.AddRange(new object[] {
            "0",
            "1",
            "2"});
            this.FrameSkip.Location = new System.Drawing.Point(73, 6);
            this.FrameSkip.Name = "FrameSkip";
            this.FrameSkip.Size = new System.Drawing.Size(121, 21);
            this.FrameSkip.TabIndex = 1;
            // 
            // FrameSkipLabel
            // 
            this.FrameSkipLabel.AutoSize = true;
            this.FrameSkipLabel.Location = new System.Drawing.Point(7, 9);
            this.FrameSkipLabel.Name = "FrameSkipLabel";
            this.FrameSkipLabel.Size = new System.Drawing.Size(60, 13);
            this.FrameSkipLabel.TabIndex = 0;
            this.FrameSkipLabel.Text = "Frame Skip";
            // 
            // Sound
            // 
            this.Sound.Controls.Add(this.SESound);
            this.Sound.Controls.Add(this.BGMSound);
            this.Sound.Controls.Add(this.SoundCheck);
            this.Sound.Location = new System.Drawing.Point(4, 22);
            this.Sound.Name = "Sound";
            this.Sound.Padding = new System.Windows.Forms.Padding(3);
            this.Sound.Size = new System.Drawing.Size(529, 136);
            this.Sound.TabIndex = 3;
            this.Sound.Text = "Sound";
            this.Sound.UseVisualStyleBackColor = true;
            // 
            // SESound
            // 
            this.SESound.AutoSize = true;
            this.SESound.Location = new System.Drawing.Point(7, 55);
            this.SESound.Name = "SESound";
            this.SESound.Size = new System.Drawing.Size(93, 17);
            this.SESound.TabIndex = 2;
            this.SESound.Text = "Sound Effects";
            this.SESound.UseVisualStyleBackColor = true;
            // 
            // BGMSound
            // 
            this.BGMSound.AutoSize = true;
            this.BGMSound.Location = new System.Drawing.Point(7, 31);
            this.BGMSound.Name = "BGMSound";
            this.BGMSound.Size = new System.Drawing.Size(115, 17);
            this.BGMSound.TabIndex = 1;
            this.BGMSound.Text = "Background Music";
            this.BGMSound.UseVisualStyleBackColor = true;
            // 
            // SoundCheck
            // 
            this.SoundCheck.AutoSize = true;
            this.SoundCheck.Location = new System.Drawing.Point(7, 7);
            this.SoundCheck.Name = "SoundCheck";
            this.SoundCheck.Size = new System.Drawing.Size(57, 17);
            this.SoundCheck.TabIndex = 0;
            this.SoundCheck.Text = "Sound";
            this.SoundCheck.UseVisualStyleBackColor = true;
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(562, 216);
            this.Controls.Add(this.TabDisplay);
            this.Controls.Add(this.OptionsCancel);
            this.Controls.Add(this.OptionsSave);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Options";
            this.Text = "Options";
            this.Load += new System.EventHandler(this.Options_Load);
            this.TabDisplay.ResumeLayout(false);
            this.OptionsGeneral.ResumeLayout(false);
            this.OptionsGeneral.PerformLayout();
            this.OptionsDisplay.ResumeLayout(false);
            this.OptionsDisplay.PerformLayout();
            this.Graphics.ResumeLayout(false);
            this.Graphics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClipDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Map)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Enemy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Shadow)).EndInit();
            this.Sound.ResumeLayout(false);
            this.Sound.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OptionsSave;
        private System.Windows.Forms.Button OptionsCancel;
        private System.Windows.Forms.TabControl TabDisplay;
        private System.Windows.Forms.TabPage OptionsGeneral;
        private System.Windows.Forms.TabPage OptionsDisplay;
        private System.Windows.Forms.Label ResolutionLabel;
        private System.Windows.Forms.ComboBox Resolution;
        private System.Windows.Forms.CheckBox WindowMode;
        private System.Windows.Forms.CheckBox WordWrap;
        private System.Windows.Forms.ComboBox Language;
        private System.Windows.Forms.Label LanguageLabel;
        private System.Windows.Forms.ComboBox FontOption;
        private System.Windows.Forms.Label FontLabel;
        private System.Windows.Forms.CheckBox CredentialsSave;
        private System.Windows.Forms.TabPage Graphics;
        private System.Windows.Forms.TabPage Sound;
        private System.Windows.Forms.CheckBox VSync;
        private System.Windows.Forms.ComboBox FrameSkip;
        private System.Windows.Forms.Label FrameSkipLabel;
        private System.Windows.Forms.CheckBox SESound;
        private System.Windows.Forms.CheckBox BGMSound;
        private System.Windows.Forms.CheckBox SoundCheck;
        private System.Windows.Forms.Label ShadowLabel;
        private System.Windows.Forms.TrackBar Shadow;
        private System.Windows.Forms.ComboBox ColorDepth;
        private System.Windows.Forms.Label ColorDepthLabel;
        private System.Windows.Forms.TrackBar Enemy;
        private System.Windows.Forms.Label EnemyLabel;
        private System.Windows.Forms.CheckBox AdvancedEffect;
        private System.Windows.Forms.TrackBar Map;
        private System.Windows.Forms.Label MapLabel;
        private System.Windows.Forms.Label FogLabel;
        private System.Windows.Forms.TrackBar ClipDistance;
        private System.Windows.Forms.Label ClipDistanceLabel;
        private System.Windows.Forms.ComboBox Fog;
        private System.Windows.Forms.CheckBox LowResolutionTextures;
        private System.Windows.Forms.CheckBox AutoFrameSkip;
    }
}